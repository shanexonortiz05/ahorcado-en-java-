/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.swing.JOptionPane;

/**
 *
 * @author Estudiante
 */
public class modelo {
    
    String [] frases = new String [5];
    String frase = new String ();
    char [] palabra;
    int [] puntos = new int[2];
    
    public modelo() {
        frases [0] = "hola como estoy";
        frases [1] = "bien y yo";
        frases [2] = "alberto plazas";
        frases [3] = "universidad distrital";
        frases [4] = "pueblo encontrado";
    }
    
    
    public int frase() {
    
        int posicionAleatoria = (int) Math.floor(Math.random()*(frases.length ));
        
        frase = frases [posicionAleatoria] ;
        int a=frase.length();
        return a;
    }
    
    public void llenar(int a){

        char [] p = new char[a];
        palabra=p;
        for(int i=0; i<=frase.length()-1; i++){
            
            if(frase.charAt(i)==' '){
                palabra[i]=' ';
            }else{
                palabra[i]='-';
            }
        }
    }
    
    public int comparar (String m) {
        int a=0;
        if(m.length()==1){
        char n=m.charAt(0);
        for(int i=0; i<=frase.length()-1; i++){           
            if(frase.charAt(i)!=' ' && n==frase.charAt(i)){
                palabra[i]=n;
                a=1;
            }
        }
        }else{
            if(m.contains(frase)){
                palabra=frase.toCharArray();
            }
        }
        return a;
    }
    
    public String mostrar(int a){
        
        String m="          AHORCADO\n\n"+
                "jugador "+a+" \n\n"+
                "la frase tiene "+frase.length()+" letras\n\n";
        
            m=m+String.valueOf(palabra);
        
        return m;
    }
    
    public boolean resultado(){
        int c=0;
        String m=String.valueOf(palabra);
            if(frase.contains(m)){
                return true;
            }
        return false;
    }
    
    public void puntos(int a, int b){
        
        switch (a){
            case 1:
                if(b==1){
                    puntos[0]=puntos[0]+5;
                }
                break;
            case 2:
                if(b==1){
                    puntos[1]=puntos[1]+5;
                }
        }
    }
        
    public String fin(){
       
                String m="          AHORCADO\n\n"+
                       "la frase era "+frase+"\n\n"+
                       "puntos:\n\n"+
                        "jugador 1: "+puntos[0]+"\n"+
                        "jugador 2: "+puntos[1];
            
                return m;
    }
    
    }
    

